import os
import sys
import time
import ffmpeg
import threading
from pytube import YouTube

SCRIPT_NAME="Video Downloader"
SCRIPT_VERSION="0.1"

TEMPORARY_PATH  = "/tmp/"
AUDIO_SAVE_PATH = "/home/santiago/Downloads/Audio/"
VIDEO_SAVE_PATH = "/home/santiago/Downloads/Video/"

MP3_EXTENSION = ".mp3"
MP4_EXTENSION = ".mp4"

DISPLAY_SIZE_FORMAT = "{} {}"

DISPLAY_SIZE_DIGITS = 2

SIZES = ["B", "KB", "MB", "GB"]

LOADING_CHAR_SEQUENCE = ["|", "/", "-", "\\"]

SUPPORTED_RESOLUTIONS_LIST = [ "144p", "240p", "360p", "480p"
                             , "720p", "1080p", "1440p", "2160p" ]

CHAR_RETURN      = "\r"
CHAR_SPACE       = " "
CHAR_EMPTY       = ""
CHAR_CHECK       = ""
CHAR_EXCLAMATION = "!"
CHAR_QUESTION    = "?"
CHAR_ARROW       = "❯"
CHAR_X           = ""
CHAR_DOT         = ""
CHAR_INFO        = "i"
CHAR_DASH        = "-"

BACKGROUND_THREAD_NAME = "ExecutingThread"

PROMPT_FORMAT  = " {} "
VERSION_FORMAT = "{} version {}"

LOADING_FORMAT = "\r{} {} {}..."
DONE_FORMAT    = "{} {} {}{}"

CONFIRM_FORMAT     = "{} Download {} - [{}] (Y/n) {} "
CONFIRM_FORMAT_ALT = "{} Download {} (Y/n) {} "

RESOLUTION_SELECTION_FORMAT = "{} Please select a video {}:"

AUDIO_DOWNLOAD_CANCELLED_FORMAT = "{} Audio download {}"
VIDEO_DOWNLOAD_CANCELLED_FORMAT = "{} Video download {}"

RESOLUTION_ITEM_FORMAT = "  {} {}"

RESOLUTION_NOT_RECOGNIZED_FORMAT = "{} Input not {}"
RESOLUTION_NOT_AVAILABLE_FORMAT  = "{} The specified resolution is not {}"

PROGRESSIVE_STREAM_FOUND_FORMAT     = "{} Progressive stream {}"
PROGRESSIVE_STREAM_NOT_FOUND_FORMAT = "{} Progressive stream not {}, downloading via DASH"

SCRIPT_HEADER_FORMAT  = "{} {}, a Youtube video downloader"
SCRIPT_USAGE_FORMAT   = "{} {} {}... {}\n"
SCRIPT_OPTIONS_FORMAT = "{}\n -h, --help\t\tShow this help message\n -V, --version\t\tDisplay script version\n -a, --audio-only\tDownload only the audio\n -n, --no-confirm\tSkip confirmation prompt\n -r, --resolution\tDownload a specific resolution"

OPTION_NOT_RECOGNIZED_FORMAT = "{} Option {} not recognized, try {} for help"
NO_URL_PROVIDED_FORMAT       = "{} No {} especified"

FILENAME_ONLY_AUDIO_FORMAT = "[Audio Only] - {}"
FILENAME_ONLY_VIDEO_FORMAT = "[Video Only] - {}"

LOADING_MESSAGE = "Loading"
DONE_MESSAGE    = "Done"

MESSAGE_FINDING_AUDIO_STREAM = "Finding best audio stream"
MESSAGE_FOUND_AUDIO_STREAM   = "Audio stream found"

MESSAGE_DOWNLOADING_AUDIO = "Downloading audio"
MESSAGE_DOWNLOADED_AUDIO  = "Audio download complete"

MESSAGE_CHECKING_RESOLUTIONS = "Checking available resolutions"
MESSAGE_CHECKED_RESOLUTIONS  = "Available resolutions checked"

MESSAGE_DOWNLOADING_VIDEO = "Downloading video"
MESSAGE_DOWNLOADED_VIDEO  = "Video download complete"

MESSAGE_JOINING_STREAMS = "Joining audio and video tracks (this may take a while)"
MESSAGE_JOINED_STREAMS  = "Audio and video tracks joined"

MESSAGE_SEARCHING_PROGRESSIVE = "Searching for a progressive stream"
MESSAGE_SEARCHED_PROGRESSIVE  = "Progressive stream search complete"

FLAG_HELP_LONG        = "--help"
FLAG_HELP_SHORT       = "-h"
FLAG_VERSION_LONG     = "--version"
FLAG_VERSION_SHORT    = "-V"
FLAG_RESOLUTION_LONG  = "--resolution"
FLAG_RESOLUTION_SHORT = "-r"
FLAG_AUDIO_ONLY_LONG  = "--audio-only"
FLAG_AUDIO_ONLY_SHORT = "-a"
FLAG_NO_CONFIRM_LONG  = "--no-confirm"
FLAG_NO_CONFIRM_SHORT = "-n"

LOADING_PRINT_DELAY = 0.15

def bold( text ):   return "\033[1m{}\033[0m".format(text)
def red( text ):    return "\033[1;91m{}\033[0m".format(text)
def green( text ):  return "\033[1;92m{}\033[0m".format(text)
def yellow( text ): return "\033[1;93m{}\033[0m".format(text)
def blue( text ):   return "\033[1;94m{}\033[0m".format(text)
def purple( text ): return "\033[1;95m{}\033[0m".format(text)

def clear_line():
    print(CHAR_RETURN + CHAR_SPACE * 100, end=CHAR_RETURN)

def display_size( number, initial_size_index=0 ):
    if number > 1000:
        number /= 1000

        return display_size( number, initial_size_index + 1 )

    return DISPLAY_SIZE_FORMAT.format( round(number, DISPLAY_SIZE_DIGITS), SIZES[initial_size_index] )

def execute( function_to_execute, loading_message=LOADING_MESSAGE, done_message=DONE_MESSAGE, quiet=False ):
    queue = []

    loading_message_verb = loading_message.split()[0]
    done_message_verb    = done_message.split()[-1]
    loading_message_text = CHAR_SPACE.join(loading_message.split()[1:])
    done_message_text    = CHAR_SPACE.join(done_message.split()[:-1])

    thread = threading.Thread(target=lambda: queue.append( function_to_execute() ), name=BACKGROUND_THREAD_NAME)
    thread.start()

    while thread.is_alive():
        if not quiet:
            for char in LOADING_CHAR_SEQUENCE:
                print( LOADING_FORMAT.format(blue(char), blue(loading_message_verb), loading_message_text), end=CHAR_EMPTY )
                time.sleep(LOADING_PRINT_DELAY)

    else:
        clear_line()

    if not quiet:
        print( DONE_FORMAT.format(green(CHAR_CHECK), done_message_text, green(done_message_verb), green(CHAR_EXCLAMATION)) )
    
    return queue[0]

def download_audio( youtube_video, no_confirm=False ):
    audio_filename = youtube_video.title + MP3_EXTENSION

    find_audio_stream_function = lambda: youtube_video.streams.get_audio_only()
    download_audio_function    = lambda: audio_stream.download(AUDIO_SAVE_PATH, filename=audio_filename)

    audio_stream = execute(find_audio_stream_function, MESSAGE_FINDING_AUDIO_STREAM, MESSAGE_FOUND_AUDIO_STREAM)
    audio_size   = display_size(audio_stream.filesize)

    confirm_prompt = CONFIRM_FORMAT.format( purple(CHAR_QUESTION), purple(youtube_video.title), bold(audio_size), purple(CHAR_ARROW) )

    _confirm_download = "yes" if no_confirm else input(confirm_prompt).lower()
    clear_line()
    
    if _confirm_download == "y" or _confirm_download == "yes" or _confirm_download == CHAR_EMPTY:
        execute(download_audio_function, MESSAGE_DOWNLOADING_AUDIO, MESSAGE_DOWNLOADED_AUDIO)

    else:
        print( AUDIO_DOWNLOAD_CANCELLED_FORMAT.format(red(CHAR_X), red("cancelled")) )

def get_supported_video_resolutions( youtube_video, quiet=False ):
    supported_resolutions_function = lambda: [ resolution for resolution in SUPPORTED_RESOLUTIONS_LIST if youtube_video.streams.filter(type="video", resolution=resolution).first() != None ]

    supported_resolutions = execute(supported_resolutions_function, MESSAGE_CHECKING_RESOLUTIONS, MESSAGE_CHECKED_RESOLUTIONS, quiet=quiet)

    return supported_resolutions

def show_resolution_selection_input( youtube_video, quiet=False ):
    supported_resolutions = get_supported_video_resolutions(youtube_video, quiet=quiet)

    print( RESOLUTION_SELECTION_FORMAT.format(purple(CHAR_QUESTION), purple("resolution")) )

    for resolution in supported_resolutions[:-1]:
        print( RESOLUTION_ITEM_FORMAT.format(bold(CHAR_DOT), resolution) )

    print( RESOLUTION_ITEM_FORMAT.format(purple(CHAR_DOT), purple(supported_resolutions[-1])) )

    _selected_resolution = input( PROMPT_FORMAT.format(blue(CHAR_ARROW)) )

    try:
        if int(_selected_resolution) - 1 in range(len(SUPPORTED_RESOLUTIONS_LIST)):
            selected_resolution = SUPPORTED_RESOLUTIONS_LIST[ int(_selected_resolution) - 1 ]

            return selected_resolution

    except ValueError:
        pass

    if _selected_resolution == CHAR_EMPTY:
        selected_resolution = supported_resolutions[-1]

    elif _selected_resolution in SUPPORTED_RESOLUTIONS_LIST:
        selected_resolution = _selected_resolution

    elif _selected_resolution + "p" in SUPPORTED_RESOLUTIONS_LIST:
        selected_resolution = _selected_resolution + "p"

    else:
        print( RESOLUTION_NOT_RECOGNIZED_FORMAT.format(red(CHAR_X), red("recognized")) )
        exit(1)

    return selected_resolution

def download_progressive_video( youtube_video, progressive_stream, no_confirm=False ):
    video_filename = youtube_video.title + MP4_EXTENSION

    download_progressive_stream_function = lambda: progressive_stream.download(VIDEO_SAVE_PATH, filename=video_filename)
    progressive_stream_size = display_size(progressive_stream.filesize)

    confirm_prompt = CONFIRM_FORMAT.format( purple(CHAR_QUESTION), purple(youtube_video.title), bold(progressive_stream_size), purple(CHAR_ARROW) )

    _confirm_download = "yes" if no_confirm else input(confirm_prompt).lower()

    if _confirm_download == "y" or _confirm_download == "yes" or _confirm_download == CHAR_EMPTY:
        execute(download_progressive_stream_function, MESSAGE_DOWNLOADING_VIDEO, MESSAGE_DOWNLOADED_VIDEO)

    else:
        print( VIDEO_DOWNLOAD_CANCELLED_FORMAT.format(red(CHAR_ARROW), red("cancelled")) )

def download_not_progressive_video( youtube_video, video_resolution, no_confirm=False ):
    video_save_path = VIDEO_SAVE_PATH + youtube_video.title + MP4_EXTENSION

    download_video_function = lambda: youtube_video.streams.filter(resolution=video_resolution).first().download(TEMPORARY_PATH, filename=FILENAME_ONLY_VIDEO_FORMAT.format(youtube_video.title))
    download_audio_function = lambda: youtube_video.streams.get_audio_only().download(TEMPORARY_PATH, filename= FILENAME_ONLY_AUDIO_FORMAT.format(youtube_video.title))

    confirm_prompt = CONFIRM_FORMAT_ALT.format( purple(CHAR_QUESTION), purple(youtube_video.title), purple(CHAR_ARROW) )

    _confirm_download = "yes" if no_confirm else input(confirm_prompt).lower()

    if _confirm_download == "y" or _confirm_download == "yes" or _confirm_download == CHAR_EMPTY:
        video_file = execute(download_video_function, MESSAGE_DOWNLOADING_VIDEO, MESSAGE_DOWNLOADED_VIDEO)
        audio_file = execute(download_audio_function, MESSAGE_DOWNLOADING_AUDIO, MESSAGE_DOWNLOADED_AUDIO)

        input_video = ffmpeg.input(video_file)
        input_audio = ffmpeg.input(audio_file)

        join_streams = lambda: ffmpeg.concat(input_video, input_audio, v=1, a=1).output(video_save_path).run(quiet=True, overwrite_output=False)

        execute(join_streams, MESSAGE_JOINING_STREAMS, MESSAGE_JOINED_STREAMS)
        os.remove(TEMPORARY_PATH + FILENAME_ONLY_VIDEO_FORMAT.format(youtube_video.title))
        os.remove(TEMPORARY_PATH + FILENAME_ONLY_AUDIO_FORMAT.format(youtube_video.title))

    else:
        print( VIDEO_DOWNLOAD_CANCELLED_FORMAT.format(red(CHAR_X), red("cancelled")) )

def download_video( youtube_video, no_confirm=False, specified_resolution=None ):
    if specified_resolution:
        if specified_resolution in get_supported_video_resolutions(youtube_video):
            video_resolution = specified_resolution

        else:
            print( RESOLUTION_NOT_AVAILABLE_FORMAT.format(red(CHAR_X), red("available")) )
            video_resolution = show_resolution_selection_input(youtube_video, quiet=True)

    else:
        video_resolution = show_resolution_selection_input(youtube_video)

    find_progressive_stream_function = lambda: youtube_video.streams.filter(progressive=True, resolution=video_resolution, type="video").first()
    
    progressive_stream = execute(find_progressive_stream_function, MESSAGE_SEARCHING_PROGRESSIVE, MESSAGE_SEARCHED_PROGRESSIVE)
    
    if progressive_stream:
        print( PROGRESSIVE_STREAM_FOUND_FORMAT.format(green(CHAR_CHECK), green("found!")) )
        download_progressive_video(youtube_video, progressive_stream, no_confirm=no_confirm)
        
    else:
        print( PROGRESSIVE_STREAM_NOT_FOUND_FORMAT.format(yellow(CHAR_INFO), yellow("found")) )
        download_not_progressive_video(youtube_video, video_resolution, no_confirm=no_confirm)

def show_help_message( arguments ):
    print( SCRIPT_HEADER_FORMAT.format(bold(SCRIPT_NAME), SCRIPT_VERSION) )
    print( SCRIPT_USAGE_FORMAT.format(red("Usage:"), arguments[0], yellow("[Options]"), green("[URL]")) )
    print( SCRIPT_OPTIONS_FORMAT.format(yellow("Options:")) )
    exit(0)

def show_version_message():
    print( VERSION_FORMAT.format(blue(SCRIPT_NAME), bold(SCRIPT_VERSION)) )
    exit(0)

def read_arguments( arguments ):
    audio_only = False
    no_confirm = False
    specified_resolution = None

    remaining_arguments = []

    for index, argument in enumerate(arguments):
        if argument[0] == CHAR_DASH:
            if argument == FLAG_HELP_LONG or argument == FLAG_HELP_SHORT:
                show_help_message(arguments)

            elif argument == FLAG_VERSION_LONG or argument == FLAG_VERSION_SHORT:
                show_version_message()

            elif argument == FLAG_RESOLUTION_LONG or argument == FLAG_RESOLUTION_SHORT:
                specified_resolution = arguments[index + 1]
                specified_resolution = specified_resolution if specified_resolution[-1] == "p" else specified_resolution + "p"
                arguments.remove( arguments[index + 1] )

            elif argument == FLAG_AUDIO_ONLY_LONG or argument == FLAG_AUDIO_ONLY_SHORT:
                audio_only = True

            elif argument == FLAG_NO_CONFIRM_LONG or argument == FLAG_NO_CONFIRM_SHORT:
                no_confirm = True

            else:
                print( OPTION_NOT_RECOGNIZED_FORMAT.format(red(CHAR_X), red(argument), yellow(FLAG_HELP_LONG)) )
                exit(1)

        else:
            remaining_arguments.append(argument)

    return audio_only, no_confirm, specified_resolution, remaining_arguments

def main():
    audio_only, no_confirm, specified_resolution, remaining_arguments = read_arguments(sys.argv)

    if len(remaining_arguments) < 2:
        print( NO_URL_PROVIDED_FORMAT.format(red(CHAR_ARROW), red("URL")) )
        exit(1)

    video_urls = [ argument for argument in remaining_arguments[1:] ]

    for video_url in video_urls:
        youtube_video = YouTube(video_url)

        if audio_only:
            download_audio(youtube_video, no_confirm)

        else:
            download_video(youtube_video, no_confirm, specified_resolution)

if __name__ == "__main__":
    main()